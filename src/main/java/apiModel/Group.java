package apiModel;

import java.time.*;

public class Group
{
    int id;
    String name;

    public Group(int id, String name, GroupMember[] members) {
        this.id = id;
        this.name = name;
        //this.created = created;
        this.members = members;
    }

    //LocalDateTime created;
    GroupMember[] members;
}
