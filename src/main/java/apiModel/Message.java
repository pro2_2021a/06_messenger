package apiModel;

import java.time.*;

public class Message
{
    private int id;
    private String text;
    private User author;
    private int groupId;
    private Instant dateTime;

    public Message(int id, String text, int groupId, User author, Instant dateTime) {
        this.id = id;
        this.text = text;
        this.dateTime = dateTime;
        this.author = author;
        this.groupId = groupId;
    }

    public int getId() {
        return id;
    }

    public int getGroupId()
    {
        return groupId;
    }

    public String getText() {
        return text;
    }


    public User getAuthor() {
        return author;
    }

    public Instant getDateTime() {
        return dateTime;
    }
}
