package apiModel;

public class GroupMember
{
    int id;
    String name;

    public GroupMember(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
