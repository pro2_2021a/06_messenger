package apiModel;

public class User
{
    public User(int id, String name, String login, UserMembership[] groups) {
        this.id = id;
        this.name = name;
        this.groups = groups;
        this.login = login;
    }

    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public UserMembership[] getGroups() {
        return groups;
    }

    public void setGroups(UserMembership[] groups) {
        this.groups = groups;
    }

    String name;
    String login;
    UserMembership[] groups;
}
