package server;
import apiModel.*;
import apiModel.Message;
import com.google.gson.*;
import server.dbModel.*;
import java.io.*;
import java.lang.reflect.Type;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ServerApp
{
    public static void Run()
    {
        try
        {
            ServerSocket serverSocket = new ServerSocket(180);
            while (true)
            {
                Socket socketConnection = serverSocket.accept();
                Thread thread = new Thread(){
                    @Override
                    public void run()
                    {
                        try
                        {
                            MsSqlMessengerDataManager msSqlMessengerDataManager = new MsSqlMessengerDataManager(
                                    "jdbc:sqlserver://database-1.cwnuam1akix6.us-east-2.rds.amazonaws.com:1194;DatabaseName=PRO2_Messenger",
                                    "PRO2Students",
                                    "3dw84fr-4354.o+4");
                            BufferedReader bufferedReader =
                                    new BufferedReader(
                                            new InputStreamReader(socketConnection.getInputStream()));
                            String firstLine = bufferedReader.readLine();
                            System.out.println(firstLine);
                            String[] split0 = firstLine.split(" ");
                            String[] split1 = split0[1].split("/");
                            OutputStream outputStream = socketConnection.getOutputStream();
                            PrintWriter printWriter = new PrintWriter(outputStream);
                            printWriter.println("HTTP/1.1 200 OK");
                            printWriter.println("Content-type: text/html; charset=utf-8");
                            printWriter.println();
                            if(split1[1].equals("groups"))
                            {
                                if(split1.length >= 4  && split1[3].equals("messages"))
                                {
                                    ReturnGroupMessages(split1[2], printWriter, msSqlMessengerDataManager);
                                }
                                else
                                {
                                    ReturnGroup(split1[2], printWriter, msSqlMessengerDataManager);
                                }
                            }
                            else if(split1[1].equals("users"))
                            {
                                ReturnUser(split1[2], printWriter, msSqlMessengerDataManager);
                            }

                            printWriter.close();
                            socketConnection.close();
                        } catch (IOException | SQLException | ClassNotFoundException e) {
                            e.printStackTrace();
                        } catch (InstantiationException e)
                        {
                            e.printStackTrace();
                        } catch (IllegalAccessException e)
                        {
                            e.printStackTrace();
                        }
                    }
                };
                thread.start();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private static void ReturnUser(String s, PrintWriter printWriter, MessengerDataManager messengerDataManager)
    {
        long userId = Long.parseLong(s);
        MessengerUser messengerUser = messengerDataManager.getUserById(userId);
        MessengerGroup[] groups = messengerDataManager.getGroups(messengerUser);
        UserMembership[] apiMemberships = new UserMembership[groups.length];
        for(int i=0; i< groups.length; i++)
        {
            apiMemberships[i] = new UserMembership((int)groups[i].getId(), groups[i].getName());
        }
        User user = new User((int)userId,messengerUser.getName(),messengerUser.getLogin(), apiMemberships);

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String result = gson.toJson(user);
        printWriter.println(result);
    }

    private static void ReturnGroup(String s, PrintWriter printWriter, MessengerDataManager messengerDataManager)
    {
        long groupId = Long.parseLong(s);
        MessengerGroup messengerGroup = messengerDataManager.getGroupById(groupId);
        MessengerUser[] users = messengerDataManager.getUsersInGroup(messengerGroup);
        GroupMember[] apiMembers = new GroupMember[users.length];
        for(int i=0; i< users.length; i++)
        {
            apiMembers[i] = new GroupMember((int) users[i].getId(), users[i].getName());
        }
        Group group = new Group((int)groupId,messengerGroup.getName(), apiMembers);

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String result = gson.toJson(group);
        printWriter.println(result);
    }

    private static void ReturnGroupMessages(String s, PrintWriter printWriter, MessengerDataManager messengerDataManager)
    {
        long groupId = Long.parseLong(s);
        MessengerGroup dbGroup = messengerDataManager.getGroup(groupId);
        server.dbModel.Message[] dbMessages = messengerDataManager.getMessages(dbGroup);

        apiModel.Message[] apiMessages = new Message[dbMessages.length];
        for(int i=0;i<dbMessages.length;i++)
        {
            MessengerUser from = dbMessages[i].getUser();
            apiMessages[i] = new Message(
                    (int) dbMessages[i].getId(),
                    dbMessages[i].getText(),
                    (int)dbMessages[i].getGroup().getId(),
                    new User((int)from.getId(),from.getName(),from.getLogin(), null),
                    dbMessages[i].getTime());
        }
        GsonBuilder gsonBuilder = new GsonBuilder().registerTypeAdapter(Instant.class, new JsonSerializer() {

            @Override
            public JsonElement serialize(Object o, Type type, JsonSerializationContext jsonSerializationContext)
            {
                return new JsonPrimitive(ZonedDateTime.ofInstant((Instant)o, ZoneId.systemDefault()).toString());
            }
        });
        Gson gson = gsonBuilder.create();
        String result = gson.toJson(apiMessages);
        printWriter.println(result);
    }
}
