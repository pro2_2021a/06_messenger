package server.dbModel;

public class MessengerUser {
    private long id;
    private String name;
    private String login;

    public MessengerUser(long id, String name, String login)
    {
        this.id = id;
        this.name = name;
        this.login = login;
    }

    public long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getLogin()
    {
        return login;
    }
}
