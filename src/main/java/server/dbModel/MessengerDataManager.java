package server.dbModel;

// DAO
public interface MessengerDataManager
{
    MessengerGroup[] getGroups(MessengerUser user);

    void createGroup(MessengerUser user, MessengerGroup group);

    Message[] getMessages(MessengerGroup group);

    MessengerGroup getGroupById(long id);

    MessengerUser getUserById(long id);

    MessengerUser[] getUsersInGroup(MessengerGroup group);

    MessengerGroup getGroup(long groupId);
}
