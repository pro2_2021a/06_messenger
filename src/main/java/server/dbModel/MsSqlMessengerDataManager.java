package server.dbModel;

import java.sql.*;
import java.util.ArrayList;

public class MsSqlMessengerDataManager implements MessengerDataManager
{
    Connection connection;
    PreparedStatement getUserByIdStatement;
    PreparedStatement getGroupByIdStatement;
    PreparedStatement getMessagesPreparedStatement;

    public MsSqlMessengerDataManager(String url, String login, String password) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); // trik pro aktivování driveru
        connection = DriverManager.getConnection(url, login, password);
        getUserByIdStatement = connection.prepareStatement("select * from messengeruser where id=?");
        getGroupByIdStatement = connection.prepareStatement("select * from messengegroup where id=?");
        getMessagesPreparedStatement = connection.
                prepareStatement("select m.ID mID, mu.ID muID, * from Message m join MessengerUser mu on m.UserID=mu.ID where m.GroupId=?");

    }

    @Override
    public MessengerGroup[] getGroups(MessengerUser user)
    {
        ArrayList<MessengerGroup> result = new ArrayList<>();
        try(Statement statement = connection.createStatement())
        {
            ResultSet resultSet= statement.executeQuery(
                    "select mg.id 'ID',mg.Name 'Name' from MessengerGroup mg inner join " +
                            "MessengerUserMessengerGroup mumg on mg.id = mumg.messengerGroupID inner join " +
                            "MessengerUser mu on mumg.MessengerUserId = mu.id " +
                            "where mu.id="+user.getId());
            while(resultSet.next())
            {
                MessengerGroup messengerGroup = new MessengerGroup(
                        resultSet.getString("Name"),
                        resultSet.getLong("ID"));
                result.add(messengerGroup);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return result.toArray(new MessengerGroup[0]);
    }

    @Override
    public void createGroup(MessengerUser user, MessengerGroup group) {

    }

    @Override
    public MessengerGroup getGroupById(long id)
    {
        MessengerGroup result;
        try(Statement statement = connection.createStatement())
        {
            getGroupByIdStatement.setLong(1,id);
            ResultSet resultSet= getUserByIdStatement.executeQuery();
            resultSet.next();
            result = new MessengerGroup(
                    resultSet.getString("Name"),
                    resultSet.getLong("ID")
            );
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return result;
    }

    @Override
    public MessengerUser getUserById(long id)
    {
        MessengerUser result;
        try(Statement statement = connection.createStatement())
        {
            getUserByIdStatement.setLong(1,id);
            ResultSet resultSet= getUserByIdStatement.executeQuery();
            resultSet.next();
            result = new MessengerUser(
                    resultSet.getLong("ID"),
                    resultSet.getString("Name"),
                    resultSet.getString("Login"));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return result;
    }

    @Override
    public MessengerUser[] getUsersInGroup(MessengerGroup group) {
        return new MessengerUser[0];
    }

    @Override
    public MessengerGroup getGroup(long groupId)
    {
        MessengerGroup result;
        try(Statement statement = connection.createStatement())
        {
            getUserByIdStatement.setLong(1, groupId);
            ResultSet resultSet= getUserByIdStatement.executeQuery();
            resultSet.next();
            result = new MessengerGroup(
                    resultSet.getString("Name"),
                    resultSet.getLong("ID"));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return result;
    }

    @Override
    public Message[] getMessages(MessengerGroup group)
    {

        ArrayList<Message> results = new ArrayList<>();
        try(Statement statement = connection.createStatement())
        {
            getMessagesPreparedStatement.setLong(1, group.getId());
            ResultSet resultSet= getMessagesPreparedStatement.executeQuery();
            while (resultSet.next())
            {
                MessengerUser from = new MessengerUser(
                        resultSet.getLong("muID"),
                        resultSet.getString("Name"),
                        resultSet.getString("Login")
                );

                Message message = new Message(
                        resultSet.getString("Text"),
                        group,
                        from,
                        resultSet.getTimestamp("Time").toInstant(),
                        resultSet.getLong("ID"));
                results.add(message);
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
        return results.toArray(new Message[0]);
    }
}
