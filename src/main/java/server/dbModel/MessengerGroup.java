package server.dbModel;

public class MessengerGroup {
    private String name;
    private long id;

    public MessengerGroup(String name, long id)
    {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public long getId()
    {
        return id;
    }
}
