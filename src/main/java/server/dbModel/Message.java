package server.dbModel;

import java.time.Instant;

public class Message {
    private String text;
    private MessengerGroup group;
    private MessengerUser user;
    private Instant time;
    private long id;

    public Message(String text, MessengerGroup group, MessengerUser user, Instant time, long id)
    {
        this.text = text;
        this.group = group;
        this.user = user;
        this.time = time;
        this.id = id;
    }

    public long getId()
    {
        return id;
    }

    public String getText()
    {
        return text;
    }

    public MessengerGroup getGroup()
    {
        return group;
    }

    public MessengerUser getUser()
    {
        return user;
    }

    public Instant getTime()
    {
        return time;
    }
}
