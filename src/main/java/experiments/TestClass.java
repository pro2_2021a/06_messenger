package experiments;

import client.gui.ClientMainFrame;
import server.ServerApp;

import javax.swing.*;
import java.io.IOException;

public class TestClass
{
    public static void main(String[] args)
    {
        // VERZE S LAMBDA VÝRAZEM
        Thread serverThread = new Thread(() -> ServerApp.Run());
        // VERZE S :: pro statické metody
        //Thread serverThread = new Thread(ServerApp::Run);
        // VERZE S ANONYMNÍ TŘÍDOU
//        Thread serverThread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                ServerApp.Run();
//            }
//        });

        serverThread.start();

        SwingUtilities.invokeLater(() -> {
            try {
                new ClientMainFrame();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
