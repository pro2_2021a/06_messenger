package client.gui;
import apiModel.UserMembership;
import javax.swing.table.AbstractTableModel;

public class MembershipsTableModel extends AbstractTableModel
{
    private UserMembership[] memberships = new UserMembership[0];

    public void set(UserMembership[] memberships)
    {
        this.memberships = memberships;
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return false;
    }

    @Override
    public String getColumnName(int column)
    {
        switch (column)
        {
            case 0:
                return "Název";
            case 1:
                return "ID";
            case 2:
                return "";
            default:
                return "?";
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        switch (columnIndex)
        {
            case 0:
                return String.class;
            case 1:
                return Integer.class;
            case 2:
                return String.class;
            default:
                return null;
        }
    }

    @Override
    public int getRowCount()
    {
        return memberships.length;
    }

    @Override
    public int getColumnCount()
    {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        UserMembership item = memberships[rowIndex];
        switch (columnIndex) {
            case 0:
                return item.name;
            case 1:
                return item.id;
            case 2:
                return "";
            default:
                return "?";
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {

    }
}
