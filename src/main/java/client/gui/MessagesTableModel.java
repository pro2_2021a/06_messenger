package client.gui;

import apiModel.Message;
import apiModel.UserMembership;

import javax.swing.table.AbstractTableModel;

public class MessagesTableModel extends AbstractTableModel
{
    private Message[] messages = new Message[0];

    public void set(Message[] messages)
    {
        this.messages = messages;
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return false;
    }

    @Override
    public String getColumnName(int column)
    {
        switch (column)
        {
            case 0:
                return "Odesláno";
            case 1:
                return "Od koho";
            case 2:
                return "Text";
            default:
                return "?";
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        switch (columnIndex)
        {
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            default:
                return null;
        }
    }

    @Override
    public int getRowCount()
    {
        return messages.length;
    }

    @Override
    public int getColumnCount()
    {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        Message item = messages[rowIndex];
        switch (columnIndex) {
            case 0:
                return item.getDateTime().toString();
            case 1:
                return item.getAuthor().getName();
            case 2:
                return item.getText();
            default:
                return "?";
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {

    }
}
